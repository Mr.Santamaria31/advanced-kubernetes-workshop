cd $HOME/advanced-kubernetes-workshop/terraform/project
gcloud iam service-accounts create terraform --display-name terraform-sa
export TERRAFORM_SA_EMAIL=$(gcloud iam service-accounts list \
    --filter="displayName:terraform-sa" \
    --format='value(email)')
export GCE_EMAIL=$(gcloud iam service-accounts list --format='value(email)' | grep compute)
export PROJECT=$(gcloud info --format='value(config.project)')
gcloud projects add-iam-policy-binding $PROJECT --role roles/owner --member serviceAccount:$TERRAFORM_SA_EMAIL
gcloud projects add-iam-policy-binding $PROJECT --role roles/owner --member serviceAccount:$GCE_EMAIL
gcloud iam service-accounts keys create credentials.json --iam-account $TERRAFORM_SA_EMAIL
cp credentials.json ../../lab1/project/.
cp credentials.json ../../lab2/project/.
cp credentials.json ../../lab3/project/.
cd $HOME
