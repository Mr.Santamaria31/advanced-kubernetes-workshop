variable "name" { default = "cluster-1" }
variable "zone" { default = "us-central1-f" }
variable "network" { default = "default" }
variable "subnetwork" {}
variable "master_ip" { default = "172.18.0.0/28" }
variable "secondary_pod" {}
variable "secondary_svc" {}
