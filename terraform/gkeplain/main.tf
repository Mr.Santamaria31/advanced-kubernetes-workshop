resource "google_container_cluster" "primary" {
  name                    = "${var.name}"
  zone                    = "${var.zone}"
  network                 = "${var.network}"
  min_master_version	  = "${var.min_master_version}"
  initial_node_count      = 3
  node_config {
    machine_type	  = "${var.machine_type}"
    image_type		  = "${var.image_type}"
  }
}
resource "null_resource" "cluster" {
   provisioner "local-exec" {
        command = "echo ${google_container_cluster.primary.project}  >> cluster-name.txt"
      }
   provisioner "local-exec" {
        command = "gcloud container clusters get-credentials ${google_container_cluster.primary.name} --zone ${google_container_cluster.primary.zone} --project ${google_container_cluster.primary.project}"
      }
   provisioner "local-exec" {
        command = "kubectx ${google_container_cluster.primary.name}=\"gke_\"${google_container_cluster.primary.project}\"_\"${google_container_cluster.primary.zone}\"_\"${google_container_cluster.primary.name}"
      }
   provisioner "local-exec" {
        command = "kubectl apply -f ~/istio-1.0.0/install/kubernetes/helm/istio/templates/crds.yaml --context ${google_container_cluster.primary.name}"
      }
   provisioner "local-exec" {
        command = "kubectl create clusterrolebinding user-admin-binding --clusterrole=cluster-admin --user=$(gcloud config get-value account) --context ${google_container_cluster.primary.name}"
      }
   provisioner "local-exec" {
        command = "kubectl create serviceaccount tiller --namespace kube-system --context ${google_container_cluster.primary.name}"
      }
   provisioner "local-exec" {
        command = "kubectl create clusterrolebinding tiller-admin-binding --clusterrole=cluster-admin --serviceaccount=kube-system:tiller --context ${google_container_cluster.primary.name}"
      }
   provisioner "local-exec" {
        command = "helm init --service-account=tiller --kube-context ${google_container_cluster.primary.name}"
      }
   provisioner "local-exec" {
        command = "sleep 20; helm install ~/istio-1.0.0/install/kubernetes/helm/istio --name istio --namespace istio-system --set tracing.enabled=true --set global.mtls.enabled=true --set grafana.enabled=true --set servicegraph.enabled=true --kube-context ${google_container_cluster.primary.name}"
      }
   depends_on = ["google_container_cluster.primary"]
}

